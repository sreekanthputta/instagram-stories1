package com.o8o4.yuchen.fragmentstatepageradapterexample;

/**
 * Created by Sreekanth Putta on 16-05-2017.
 */

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.o8o4.yuchen.fragmentstatepageradapterexample.stores.amazon;
import com.o8o4.yuchen.fragmentstatepageradapterexample.stores.flipkart;
import com.o8o4.yuchen.fragmentstatepageradapterexample.stores.snapdeal;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Objects;

import com.o8o4.yuchen.fragmentstatepageradapterexample.bubbles.*;

import static android.support.v7.appcompat.R.id.info;

public class MyAccessibilityService extends AccessibilityService {
    AccessibilityServiceInfo info = new AccessibilityServiceInfo();
    private static final String TASK_LIST_VIEW_CLASS_NAME = "com.example.android.apis.accessibility.TaskListView";
    ArrayList<AccessibilityNodeInfo> Nodes;
    String PreviousProduct = "";
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor;
    WindowManager wm;
    int x=0,y=0;
    View view=null;
    WindowManager.LayoutParams params;

    Boolean isBubbleOpen = false;

    BubblesManager bubblesManager = null;
    BubbleLayout bubbleView = null;
    DisplayMetrics metrics = new DisplayMetrics();
    Boolean isWmEmpty = true;

    String store ="";

    @Override
    public void onCreate() {
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPrefs.edit();
        metrics = getApplicationContext().getResources().getDisplayMetrics();
        bubblesManager = new BubblesManager.Builder(this)
                .setTrashLayout(R.layout.bubble_trash_layout)
                .build();
        bubblesManager.initialize();
        wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.overlay_layout, null);
        params = new WindowManager.LayoutParams(
                metrics.widthPixels * 7 / 8, metrics.heightPixels * 3 / 4,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        params.y = metrics.heightPixels / 20;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR
                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM; // assigning some flags to the layout
        params.windowAnimations = android.R.style.Animation_Toast; // adding an animation to it.

        super.onCreate();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        final int eventType = event.getEventType();
        String eventText = null;
        switch (eventType) {
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                try {
                    if (getRootInActiveWindow().getPackageName().equals("in.amazon.mShop.android.shopping")) {
                        store = "Amazon";
                        showBubble();
                        //PreviousProduct = amazonFunction(getRootInActiveWindow(), PreviousProduct);
                    } else if (getRootInActiveWindow().getPackageName().equals("com.snapdeal.main")) {
                        store = "Snapdeal";
                        showBubble();
                        //PreviousProduct = snapdealFunction(getRootInActiveWindow(), PreviousProduct);
                    } else if (getRootInActiveWindow().getPackageName().equals("com.flipkart.android")) {
                        PreviousProduct = flipkartFunction(getRootInActiveWindow(), PreviousProduct);
                    } else {
                        Log.e(isBubbleOpen + "  " + getRootInActiveWindow().getPackageName() + "    gkhkhkjhkhk", "\n" + event.getSource() + "\n" + findFocus(1) + "\n" + findFocus(2) + "\n" + getWindows() + "\n1234" + getRootInActiveWindow() + "\n" + getServiceInfo() + "\n\n\n");
                        PreviousProduct = "";
                        removeBubble();
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
                break;
            case AccessibilityEvent.TYPE_ANNOUNCEMENT:
                //Log.e("gkhkhkjhkhk","\n"+event.getSource() +"\n"+findFocus(1)+"\n"+findFocus(2)+"\n"+getWindows()+"\n"+getRootInActiveWindow()+"\n"+getServiceInfo()+"\n\n\n");
                Nodes = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityNodeInfo rootNode = getRootInActiveWindow();
                try {
                    findChildViews(rootNode);

                    for (AccessibilityNodeInfo mNode : Nodes) {
                        //Log.d("svhjsbhjsb", mNode.getContentDescription()+", "+mNode.getViewIdResourceName());
                        if (mNode.getViewIdResourceName() != null && mNode.getViewIdResourceName().contains("title")) {
                            //Log.d("xckkjnjknkjnnfv", mNode.getContentDescription()+"");
                            //return;
                        }
                        if (mNode.getViewIdResourceName() != null || true) {
                            //Log.d("ghvjjhgjh", mNode.getText()+", "+mNode.getViewIdResourceName()+", "+mNode.getClassName()+", "+mNode);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

        eventText = eventText + event.getContentDescription();

        // Do something nifty with this text, like speak the composed string
        // back to the user.
        //Log.d("dfvdfvsdfsdf", event + "");
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    public void onServiceConnected() {
        // Set the type of events that this service wants to listen to. Others won't be passed to this service.
        // We are only considering windows state changed event.
        info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
        // If you only want this service to work with specific applications, set their package names here. Otherwise, when the service is activated, it will listen to events from all applications.
        //info.packageNames = new String[]{"com.flipkart.android"};
        // Set the type of feedback your service will provide. We are setting it to GENERIC.
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        // Default services are invoked only if no package-specific ones are present for the type of AccessibilityEvent generated.
        // This is a general-purpose service, so we will set some flags
        info.flags |= AccessibilityServiceInfo.DEFAULT;
        info.flags |= AccessibilityServiceInfo.FLAG_INCLUDE_NOT_IMPORTANT_VIEWS;
        info.flags |= AccessibilityServiceInfo.FLAG_REPORT_VIEW_IDS;
        info.flags |= AccessibilityServiceInfo.FLAG_REQUEST_ENHANCED_WEB_ACCESSIBILITY;
        info.flags |= AccessibilityServiceInfo.FLAG_RETRIEVE_INTERACTIVE_WINDOWS;
        // We are keeping the timeout to 0 as we don’t need any delay or to pause our accessibility events
        info.notificationTimeout = 0;
        Log.d(info + "", "fxvdfvsdfsgdfs");
        this.setServiceInfo(info);
    }

//    private AccessibilityNodeInfo getListItemNodeInfo(AccessibilityNodeInfo source) {
//        AccessibilityNodeInfo current = source;
//        while (true) {
//            AccessibilityNodeInfo parent = current.getParent();
//            if (parent == null) {
//                return null;
//            }
//            if (TASK_LIST_VIEW_CLASS_NAME.equals(parent.getClassName())) {
//                return current;
//            }
//            // NOTE: Recycle the infos.
//            AccessibilityNodeInfo oldCurrent = current;
//            current = parent;
//            oldCurrent.recycle();
//        }
//    }

    protected void findChildViews(AccessibilityNodeInfo parentView) {
        int childCount = parentView.getChildCount();

        if (parentView == null || parentView.getClassName() == null) {
            return;
        }

        if (childCount == 0) {
            Nodes.add(parentView);
        } else {
            for (int i = 0; i < childCount; i++) {
                findChildViews(parentView.getChild(i));
            }
        }
    }


    public String amazonFunction(AccessibilityNodeInfo root, String PreviousProduct) {
        try {
            Nodes = new ArrayList<AccessibilityNodeInfo>();
            findChildViews(root);
            for (AccessibilityNodeInfo mNode : Nodes) {
                if (mNode.getViewIdResourceName() != null && (mNode.getViewIdResourceName().equals("title") || mNode.getViewIdResourceName().equals("title_feature_div"))) {
                    PreviousProduct = mNode.getContentDescription().toString();
                    Log.d("Amazon", PreviousProduct);
                    //showBubble();
                    return PreviousProduct;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String snapdealFunction(AccessibilityNodeInfo root, String PreviousProduct) {
        try {
            Nodes = new ArrayList<AccessibilityNodeInfo>();
            findChildViews(root);
            for (AccessibilityNodeInfo mNode : Nodes) {
                if (mNode.getViewIdResourceName() != null && mNode.getViewIdResourceName().toString().equals("com.snapdeal.main:id/ptitleView") && !mNode.getText().toString().equals(PreviousProduct)) {
                    PreviousProduct = mNode.getText().toString();
                    Log.d("Snapdeal", PreviousProduct);

                    showBubble();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return PreviousProduct;
    }

    public String flipkartFunction(AccessibilityNodeInfo root, String PreviousProduct) {
        String Text = "";
        try {
            Nodes = new ArrayList<AccessibilityNodeInfo>();
            findChildViews(root);
            boolean isProductPage = false;
            for (AccessibilityNodeInfo mNode : Nodes) {
                if (!isProductPage && mNode.getViewIdResourceName() != null && mNode.getViewIdResourceName().equals("com.flipkart.android:id/productImage")) {
                    Log.d("ProductPage", "sdfdfsdfddd");
                    isProductPage = true;
                }
                if (isProductPage && mNode.getText() != null) {
                    if ((mNode.getText().equals("Special Price") || mNode.getText().equals("Ends in few hours") || mNode.getText().equals("₹")) && mNode.isVisibleToUser()) {
                        if (!Text.equals(PreviousProduct)) {
                            Log.d("FlipkartProduct", Text);
                            showBubble();
                        }
                        return Text;
                    } else {
                        Text = mNode.getText().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return PreviousProduct;
    }

    void showBubble() {
        if (!isBubbleOpen)
            showLayoutOnScreen();
    }

    void removeBubble() {
        if (isBubbleOpen) {
            bubblesManager.removeBubble(bubbleView);
            isBubbleOpen = false;
            if(!isWmEmpty){
                wm.removeView(view);
                isWmEmpty = true;
                //view = null;
            }
        }
    }

    protected void showLayoutOnScreen() {
//        if (bubblesManager == null) {
//            bubblesManager = new BubblesManager.Builder(this)
//                    .setTrashLayout(R.layout.bubble_trash_layout)
//                    .build();
//            bubblesManager.initialize();
//        }
        bubbleView = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.bubble_layout, null);
        bubbleView.setShouldStickToWall(true);
        bubbleView.setOnBubbleRemoveListener(new BubbleLayout.OnBubbleRemoveListener() {
            @Override
            public void onBubbleRemoved(BubbleLayout bubble) {
                isBubbleOpen = false;
            }
        });

        bubbleView.setOnBubbleClickListener(new BubbleLayout.OnBubbleClickListener() {
            @Override
            public void onBubbleClick(BubbleLayout bubble) {
                if (isWmEmpty) {
                    bubbleView.isMovable = false;
                    bubbleView.moveto(50, metrics.heightPixels * 16 / 20);
                    x = bubbleView.getViewParams().x;
                    y = bubbleView.getViewParams().y;
                    wm.addView(view, params);
                    overlayFuntion();
                    isWmEmpty = false;
                } else {
                    wm.removeView(view);
                    isWmEmpty = true;
                    //view = null;
                    bubbleView.isMovable = true;
                    bubbleView.moveto(x, y);
                }
            }
        });
        bubblesManager.initialize();
        if(!bubblesManager.addBubble(bubbleView, metrics.widthPixels - 600, metrics.heightPixels - 200)) {
            new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(1000);
                        bubblesManager.addBubble(bubbleView, metrics.widthPixels - 600, metrics.heightPixels - 200);
                        isBubbleOpen = true;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }else{
            isBubbleOpen = true;
        }
    }

    private void overlayFuntion() {
        if(store == "Amazon")PreviousProduct = amazonFunction(getRootInActiveWindow(), PreviousProduct);
        else if(store == "Snapdeal")PreviousProduct = snapdealFunction(getRootInActiveWindow(), PreviousProduct);
        ((TextView) view.findViewById(R.id.overlay_product_name)).setText(PreviousProduct);
        ((TextView) view.findViewById(R.id.overlay_store)).setText(store);
    }

}