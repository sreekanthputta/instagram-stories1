package com.o8o4.yuchen.fragmentstatepageradapterexample.stores;

import android.content.Context;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Sreekanth Putta on 19-05-2017.
 */

public class snapdeal {
    private static final String TASK_LIST_VIEW_CLASS_NAME = "com.example.android.apis.accessibility.TaskListView";
    ArrayList<AccessibilityNodeInfo> Nodes;

    public String snapdealFunction(AccessibilityNodeInfo root, String PreviousProduct){
        try {
            Nodes = new ArrayList<AccessibilityNodeInfo>();
            findChildViews(root);
            for (AccessibilityNodeInfo mNode : Nodes) {
                if(mNode.getViewIdResourceName()!=null&&mNode.getViewIdResourceName().toString().equals("com.snapdeal.main:id/ptitleView")&&!mNode.getText().toString().equals(PreviousProduct)){
                    PreviousProduct = mNode.getText().toString();
                    Log.d("Snapdeal", PreviousProduct);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return PreviousProduct;
    }

    protected  void findChildViews(AccessibilityNodeInfo parentView) {
        int childCount = parentView.getChildCount();

        if (parentView == null || parentView.getClassName() == null) {
            return;
        }

        if (childCount == 0 ) {
            Nodes.add(parentView);
        } else {
            for (int i = 0; i < childCount; i++) {
                findChildViews(parentView.getChild(i));
            }
        }
    }
}
