package com.o8o4.yuchen.fragmentstatepageradapterexample;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuchen on 15-08-30.
 */
public class ArrayListFragment extends Fragment {
    int mNum;
    //RoundCornerProgressBar progressBar;
    CountDownTimer countDownTimer;
    long millisFinished, millisFinished1;
    OnScrollFinishListener scrollFinishListener;
    long timeStamp;
    static Context Context;
    static int NUM_ITEMS;
    static int NUM_IMAGES;
    ViewPager vp;
    ArrayList<RoundCornerProgressBar> progressBars = new ArrayList<RoundCornerProgressBar>();
    static int currentImage=0;
    ImageView image;

    private static List<String[]> list = new ArrayList<String[]>();

    static ArrayListFragment newInstance(int num, int NUM_items, int NUM_images, List<String[]> List) {
        ArrayListFragment f = new ArrayListFragment();

        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        NUM_ITEMS = NUM_items;
        NUM_IMAGES = NUM_images;

        list = List;
        return f;
    }

    static void setContext(Context context){
        Context = context;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments() != null ? getArguments().getInt("num") : 1;
    }

    public void  setScrollFinishListener(OnScrollFinishListener listener){
        this.scrollFinishListener = listener;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            try{
                currentImage = 0;
                for(int i=0; i<NUM_IMAGES; i++){
                    progressBars.get(i).setProgress(0);
                }
                millisFinished=0;
                millisFinished1=5000;
                countDownTimer.cancel();
                countDownTimer.start();
            }catch (Exception e){
                try{
                    final Thread thread=  new Thread(){
                        @Override
                        public void run(){
                            try{
                                try {
                                    synchronized (this) {
                                        wait(1000);
                                    }
                                } catch (InterruptedException ex) {}
                                millisFinished = 0;
                                millisFinished1 = 5000;
                                countDownTimer.cancel();
                                countDownTimer.start();
                            }catch(Exception ee){}

                        }
                    };
                    thread.start();
//                    while(true){
//                        try{
//                            thread.start();
//                        }catch (Exception ee){
//                            continue;
//                        }
//                        break;
//                    }
                }catch (Exception eee){}

            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_pager_list, container, false);
        View tv = v.findViewById(R.id.text);
        ((TextView)tv).setText(list.get(0)[mNum]);
        //progressBar = (RoundCornerProgressBar) v.findViewById(R.id.progressbar);
        LinearLayout linearProgress = (LinearLayout) v.findViewById(R.id.progressbarLinearLayout);
        for (int i=0; i < NUM_IMAGES; i++) {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    8,
                    1.0f
            );
            LayoutInflater layoutInflater1 = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View progressBar2 = layoutInflater1.inflate(R.layout.progress_bar_layout, container, false);
            final RoundCornerProgressBar progressbar2 = (RoundCornerProgressBar) progressBar2.findViewById(R.id.progressbar);
            progressbar2.setProgress(0);
            progressbar2.setPadding(10,0,10,0);
            progressbar2.setLayoutParams(param);
            linearProgress.addView(progressbar2);
            progressBars.add(progressbar2);
        }
        vp=(ViewPager) getActivity().findViewById(R.id.pager);
        countDownTimer = new CountDownTimer(5000, 10) {

            @Override
            public void onTick(long millisUntilFinished_) {
                if(vp.getCurrentItem()==mNum) {
                    millisFinished = millisUntilFinished_;
                    double perc = 100.0;
                    double progress = (5000 - millisUntilFinished_) / 5000.0 * perc;
                    progressBars.get(currentImage).setProgress((float) progress);
                }
                else {
                    countDownTimer.cancel();
                    progressBars.get(currentImage).setProgress(0);
                }
            }

            @Override
            public void onFinish() {
                nextImage();
            }
        };
        image = (ImageView) v.findViewById(R.id.image);
        //image.setImageResource(Integer.parseInt(list.get(1)[mNum]));
        TextView oldPrice = (TextView) v.findViewById(R.id.oldPrice1);
        TextView newPrice = (TextView) v.findViewById(R.id.newPrice1);
        TextView discount = (TextView) v.findViewById(R.id.discountpercent);
        oldPrice.setText(" ₹"+list.get(2)[mNum]+"/- ");
        newPrice.setText("₹"+list.get(3)[mNum]+"/-");
        discount.setText(String.valueOf(Math.round((100-Float.valueOf(list.get(3)[mNum].replaceAll(",", ""))/Float.valueOf(list.get(2)[mNum].replaceAll(",", ""))*100))));
        TextView shopNow = (TextView) v.findViewById(R.id.shopNow);
        shopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                Pattern pattern = Pattern.compile("^(?:https?:\\/\\/)?(?:[^@\n]+@)?(?:www\\.)?([^:\\/\n.]+)");
                Matcher m = pattern.matcher(list.get(4)[mNum]);
                Log.d(list.get(4)[mNum], "^(?:https?:\\/\\/)?(?:[^@\n]+@)?(?:www\\.)?([^:\\/\n.]+)");
                Log.d("fvdxfv",m.group(1));
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(4)[mNum]));
                startActivity(intent);
            }
        });
        RelativeLayout l = (RelativeLayout) v.findViewById(R.id.relativeLayoutPager);
        l.setOnTouchListener(new View.OnTouchListener() {
            int X=-1,Y=-1;
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    X = (int) event.getX();
                    Y = (int) event.getY();
                    timeStamp = System.currentTimeMillis();
                    countDownTimer.cancel();
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    if(System.currentTimeMillis()-timeStamp<150 && X == (int) event.getX() && Y == (int) event.getY()){
                        if(scrollFinishListener!=null){
                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            ((Activity) Context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            if((float)X/displayMetrics.widthPixels>0.3){
                                nextImage();
                            }
                            else if(mNum>0||currentImage>0){
                                prevImage();
                            }
                            else
                                resumeCountDown();
                        }
                    }
                    else{
                        resumeCountDown();
                    }
                }
                return true;
            }
        });
        CircleImageView profile_image = (CircleImageView) v.findViewById(R.id.profile_image);
        profile_image.setImageResource(R.drawable.abc_ab_share_pack_mtrl_alpha);
        ImageView imageView3 = (ImageView) v.findViewById(R.id.imageView3);
        ImageView imageView4 = (ImageView) v.findViewById(R.id.imageView4);
        imageView3.setColorFilter(Color.parseColor("#FDD331"));
        imageView4.setColorFilter(Color.parseColor("#2670E9"));
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void resumeCountDown(){
        millisFinished1 = millisFinished;
        countDownTimer = new CountDownTimer(5000, 10) {
            @Override
            public void onTick(long millisUntilFinished_) {
                if(vp.getCurrentItem()==mNum) {
                    millisUntilFinished_ -= 5000-millisFinished1;
                    millisFinished = millisUntilFinished_;
                    double perc = 100.0;
                    double progress = (5000 - millisUntilFinished_) / 5000.0 * perc;
                    if(progress>=100){
                        countDownTimer.cancel();
                        countDownTimer.onFinish();
                    }
                    progressBars.get(currentImage).setProgress((float) progress);
                }
                else {
                    countDownTimer.cancel();
                    progressBars.get(currentImage).setProgress(0);
                }
            }

            @Override
            public void onFinish() {
                nextImage();
            }
        };
        countDownTimer.start();
    }

    public void nextImage(){
        if(++currentImage<NUM_IMAGES){
            progressBars.get(currentImage-1).setProgress(100);
            millisFinished=0;
            millisFinished1=5000;
            countDownTimer.start();
            image.setImageResource(Integer.parseInt(list.get(1)[currentImage]));
        }
        else if(scrollFinishListener!=null&&mNum+1<NUM_ITEMS){
            currentImage = 0;
            scrollFinishListener.onScrollFinish(mNum+1);
        }
        else{
            ((Activity)Context).finish();
        }
    }

    public void prevImage(){
        if(currentImage>0){
            progressBars.get(currentImage).setProgress(0);
            currentImage--;
            image.setImageResource(Integer.parseInt(list.get(1)[currentImage]));
            millisFinished=0;
            millisFinished1=5000;
            countDownTimer.start();
        }
        else{
            scrollFinishListener.onScrollFinish(mNum-1);
        }
    }

    public interface OnScrollFinishListener{
        void onScrollFinish(int nextPage);
    }

}
