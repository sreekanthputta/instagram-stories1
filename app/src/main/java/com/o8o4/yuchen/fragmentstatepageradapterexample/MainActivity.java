package com.o8o4.yuchen.fragmentstatepageradapterexample;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.o8o4.yuchen.fragmentstatepageradapterexample.bubbles.BubbleLayout;
import com.o8o4.yuchen.fragmentstatepageradapterexample.bubbles.BubblesManager;

import java.util.ArrayList;
import java.util.List;

import static android.view.KeyEvent.ACTION_UP;
import static android.view.KeyEvent.KEYCODE_BACK;

public class MainActivity extends Activity {
    private RecyclerView recyclerView;
    private RecyclerViewAdapter mAdapter;
    private List<item> itemList = new ArrayList<>();

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stories_icons);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new RecyclerViewAdapter(itemList, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        AccessibilityManager manager = (AccessibilityManager) this.getSystemService(Context.ACCESSIBILITY_SERVICE);
        if (!manager.isEnabled()) {
            startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
        }

        prepareItemData();



        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                stopService(new Intent(getApplicationContext(),FloatingService.class));
//                Intent intent= new Intent(getApplicationContext(),FloatingService.class);
//                intent.putExtra("ProductName", "shdj");
//                intent.putExtra("Store", "sdg");
//                startService(intent);

//                if (bubblesManager == null)
//                    showLayoutOnScreen("zgdycg");
//                else
//                    bubbleView.moveto(50, 60);

            }
        });


    }

    private void prepareItemData() {
        item item = new item("Shoes", R.drawable.clock1);
        itemList.add(item);

        item = new item("Electronics", R.drawable.fashion1);
        itemList.add(item);

        item = new item("School", R.drawable.heels1);
        itemList.add(item);

        item = new item("College", R.drawable.jeans1);
        itemList.add(item);

        item = new item("Men Shirts", R.drawable.laptop);
        itemList.add(item);

        item = new item("Men Trousers", R.drawable.shirt);
        itemList.add(item);

        item = new item("Shoes", R.drawable.smartphone);
        itemList.add(item);

        item = new item("Electronics", R.drawable.tshirt);
        itemList.add(item);

        item = new item("School", R.drawable.watches);
        itemList.add(item);

        item = new item("College", R.drawable.wdress);
        itemList.add(item);
        mAdapter.notifyDataSetChanged();
    }





    @Override
    public void onDestroy() {
        super.onDestroy();
        //bubblesManager.recycle();
    }
}