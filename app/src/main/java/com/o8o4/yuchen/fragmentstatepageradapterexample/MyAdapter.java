package com.o8o4.yuchen.fragmentstatepageradapterexample;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.o8o4.yuchen.fragmentstatepageradapterexample.transformer.CubeOutTransformer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuchen on 15-08-30.
 */
public class MyAdapter extends FragmentStatePagerAdapter
{
    static final int NUM_ITEMS = 7;
    static final int NUM_IMAGES = 5;
    private ArrayList<Integer> page_indexes;
    ArrayListFragment.OnScrollFinishListener mListener;
    Context context;
    List<String[]> list = new ArrayList<String[]>();

    public MyAdapter(FragmentManager fm, ArrayListFragment.OnScrollFinishListener listener, Context context, List<String[]> list) {
        super(fm);

        page_indexes = new ArrayList<>();
        for (int i = 0; i < NUM_ITEMS; i++) {
            page_indexes.add(new Integer(i));
        }
        this.mListener = listener;
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return page_indexes.size();
    }

    @Override
    public Fragment getItem(int position) {
        Integer index = page_indexes.get(position);
        ArrayListFragment fragment =  ArrayListFragment.newInstance(index.intValue(), NUM_ITEMS, NUM_IMAGES, list);
        ArrayListFragment.setContext(context);
        fragment.setScrollFinishListener(mListener);
        return  fragment;
    }


    // This is called when notifyDataSetChanged() is called
    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }


}