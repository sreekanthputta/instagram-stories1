package com.o8o4.yuchen.fragmentstatepageradapterexample.transformer;

import android.util.FloatMath;
import android.util.Log;
import android.view.View;

import com.o8o4.yuchen.fragmentstatepageradapterexample.ABaseTransformer;

import static java.lang.Math.abs;

/**
 * Created by Sreekanth Putta on 04-05-2017.
 */
public class CubeOutTransformer extends ABaseTransformer {
    @Override
    protected void onTransform(View view, float position) {
        view.setPivotX(position < 0f ? view.getWidth() : 0f);
        view.setPivotY(view.getHeight() * 0.5f);
        float a = 90f * position*position;
        view.setRotationY(position>0 ? a : -a);
    }

    @Override
    public boolean isPagingEnabled() {
        return true;
    }

}
