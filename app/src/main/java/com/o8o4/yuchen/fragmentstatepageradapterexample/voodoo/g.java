package com.o8o4.yuchen.fragmentstatepageradapterexample.voodoo;

/**
 * Created by Sreekanth Putta on 18-05-2017.
 */

import android.view.accessibility.AccessibilityEvent;

import com.o8o4.yuchen.fragmentstatepageradapterexample.MyAccessibilityService;

public abstract interface g
{
    public abstract boolean handleEvent(AccessibilityEvent paramAccessibilityEvent);

    public abstract void setServiceObject(MyAccessibilityService paramVoodooService);
}
