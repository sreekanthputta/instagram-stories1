package com.o8o4.yuchen.fragmentstatepageradapterexample;

/**
 * Created by Sreekanth Putta on 09-05-2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private List<item> itemList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView image;
        public int position;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.iconText);
            image = (ImageView) view.findViewById(R.id.iconImage);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent a = new Intent(context, Stories.class);
                    a.putExtra("position", position);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(a);
                }
            });
        }
    }

    public RecyclerViewAdapter(List<item> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stories_icons_circularimageview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        item item = itemList.get(position);
        holder.position = position;
        holder.name.setText(item.getName());
        holder.image.setImageResource(item.getImage());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}