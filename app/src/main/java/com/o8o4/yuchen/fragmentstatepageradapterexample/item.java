package com.o8o4.yuchen.fragmentstatepageradapterexample;

/**
 * Created by Sreekanth Putta on 09-05-2017.
 */

public class item {
    private String name;
    private int image;
    private int position;

    public item() {
    }

    public item(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}