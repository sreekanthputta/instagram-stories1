package com.o8o4.yuchen.fragmentstatepageradapterexample.stores;

import android.content.Context;
import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

import com.o8o4.yuchen.fragmentstatepageradapterexample.MainActivity;

import java.util.ArrayList;

/**
 * Created by Sreekanth Putta on 19-05-2017.
 */

public class amazon {
    private static final String TASK_LIST_VIEW_CLASS_NAME = "com.example.android.apis.accessibility.TaskListView";
    ArrayList<AccessibilityNodeInfo> Nodes;

    public String amazonFunction(AccessibilityNodeInfo root, String PreviousProduct){
        try {
            Nodes = new ArrayList<AccessibilityNodeInfo>();
            findChildViews(root);
            for (AccessibilityNodeInfo mNode : Nodes) {
                if(mNode.getViewIdResourceName()!=null&&(mNode.getViewIdResourceName().equals("title")||mNode.getViewIdResourceName().equals("title_feature_div"))&&!mNode.getContentDescription().equals(PreviousProduct)){
                    PreviousProduct = mNode.getContentDescription().toString();
                    Log.d("Amazon", PreviousProduct);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return PreviousProduct;
    }

    protected  void findChildViews(AccessibilityNodeInfo parentView) {
        int childCount = parentView.getChildCount();

        if (parentView == null || parentView.getClassName() == null) {
            return;
        }

        if (childCount == 0 ) {
            Nodes.add(parentView);
        } else {
            for (int i = 0; i < childCount; i++) {
                findChildViews(parentView.getChild(i));
            }
        }
    }
}
