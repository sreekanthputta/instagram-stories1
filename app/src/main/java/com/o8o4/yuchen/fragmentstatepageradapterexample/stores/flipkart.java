package com.o8o4.yuchen.fragmentstatepageradapterexample.stores;

import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.ArrayList;

/**
 * Created by Sreekanth Putta on 19-05-2017.
 */

public class flipkart {
    private static final String TASK_LIST_VIEW_CLASS_NAME = "com.example.android.apis.accessibility.TaskListView";
    ArrayList<AccessibilityNodeInfo> Nodes;

    public String flipkartFunction(AccessibilityNodeInfo root, String PreviousProduct) {
        String Text = "";
        try {
            Nodes = new ArrayList<AccessibilityNodeInfo>();
            findChildViews(root);
            boolean isProductPage = false;
            for (AccessibilityNodeInfo mNode : Nodes) {
                if (!isProductPage && mNode.getViewIdResourceName() != null && mNode.getViewIdResourceName().equals("com.flipkart.android:id/productImage")) {
                    Log.d("ProductPage", "sdfdfsdfddd");
                    isProductPage = true;
                }
                if (isProductPage && mNode.getText() != null) {
                    if ((mNode.getText().equals("Special Price") || mNode.getText().equals("Ends in few hours") || mNode.getText().equals("₹"))&&mNode.isVisibleToUser()) {
                        if (!Text.equals(PreviousProduct)) {
                            Log.d("FlipkartProduct", Text);
                        }
                        return Text;
                    } else {
                        Text = mNode.getText().toString();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return PreviousProduct;
    }

    protected void findChildViews(AccessibilityNodeInfo parentView) {
        int childCount = parentView.getChildCount();

        if (parentView == null || parentView.getClassName() == null) {
            return;
        }

        if (childCount == 0) {
            Nodes.add(parentView);
        } else {
            for (int i = 0; i < childCount; i++) {
                findChildViews(parentView.getChild(i));
            }
        }
    }
}
