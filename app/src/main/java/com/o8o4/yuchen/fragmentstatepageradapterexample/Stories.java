package com.o8o4.yuchen.fragmentstatepageradapterexample;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.WindowManager;

import com.o8o4.yuchen.fragmentstatepageradapterexample.transformer.CubeOutTransformer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sreekanth Putta on 09-05-2017.
 */

public class Stories extends FragmentActivity implements ArrayListFragment.OnScrollFinishListener {

    MyAdapter mAdapter;
    ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pager);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ArrayListFragment.OnScrollFinishListener listener = new ArrayListFragment.OnScrollFinishListener() {
            @Override
            public void onScrollFinish(int nextPage) {
                mPager.setCurrentItem(nextPage);
            }
        };

        List<String[]> list = new ArrayList<String[]>();
        String[] names = {"Being Fab Men's Solid Casual Brown Shirt","Alan Jones Striped Men's Round Neck Dark Blue T-Shirt","Rodid Men's Checkered Casual Red, Blue Shirt","Rodid Solid Men's Round Neck Black T-Shirt","Roadster Full Sleeve Solid Men's Sweatshirt","Harvard Slim Fit Men's Brown Trousers","Alan Jones Striped Men's Round Neck Dark Blue T-Shirt"};
        String[] images = {String.valueOf(R.drawable.android), String.valueOf(R.drawable.ios), String.valueOf(R.drawable.win10), String.valueOf(R.drawable.one), String.valueOf(R.drawable.two), String.valueOf(R.drawable.three), String.valueOf(R.drawable.four)};
        String[] oldPrices = {"19,999","9,999","14,999","24,999","29,999","59,999","89,999"};
        String[] newPrices = {"17,999","7,999","10,499","18,749","14,999","23,999","45,999"};
        String[] links = {"http://www.google.com","http://www.google.com","http://www.google.com","http://www.google.com","http://www.google.com","http://www.google.com","http://www.google.com"};
        list.add(names);
        list.add(images);
        list.add(oldPrices);
        list.add(newPrices);
        list.add(links);

        mAdapter = new MyAdapter(getSupportFragmentManager(), listener, this, list);

        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mPager.setPageTransformer(true,new CubeOutTransformer());
        int a = getIntent().getExtras().getInt("position");
        mPager.setCurrentItem(a);
    }

    @Override
    public void onScrollFinish(int nextPage) {
        mPager.setCurrentItem(nextPage);
    }
}